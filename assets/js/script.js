/* ################################################################################################################## */
/* Define all parameters for the API Connection */
const typeOfProduct = "cameras";
let APIURL = "http://51.77.149.191:3000/api/" + typeOfProduct + "/";
let idProduct = "";
let products = [];
let contact;

if (localStorage.getItem("userCard"))
{
    console.log("[Admin] - User's card exist");
}
else
{
    console.log("[Admin] - User's card doesn't exist, it will be created in local Storage");
    let cardInit = [];
    localStorage.setItem("userCard", JSON.stringify(cardInit));
}
let userCard = JSON.parse(localStorage.getItem("userCard"));
/* ################################################################################################################## */

/* retrieves datas from API */
getProducts = () =>
{
    return new Promise((resolve) =>
    {
       let request = new XMLHttpRequest();
       request.onreadystatechange = function ()
       {
           if (this.readyState === XMLHttpRequest.DONE && this.status === 200)
           {
               resolve(JSON.parse(this.responseText));
               console.log("[Admin] - API connection ok")
               let error = document.getElementById("error");
               if (error)
               {
                   error.remove();
               }
           }
           else
           {
               console.log("[Admin] - Error on API connection");
           }
       }
       request.open("GET", APIURL + idProduct);
       request.send();
    });
}

/* generates all products card in the main page */
async function allProductsList()
{
    const products = await getProducts();
    let titleProductSection = document.createElement("h2")
    let listProducts = document.getElementById("list-products");

    titleProductSection.setAttribute("class", "col text-center mb-4");
    titleProductSection.textContent = "Nos produits en vente";
    document.getElementById("title-products").appendChild(titleProductSection);

    products.forEach((product) =>
    {
        /* Generate all of elements of a card */
        let productCard = document.createElement("div");
        let productImg = document.createElement("img");
        let productInfos = document.createElement("div");
        let productTitle = document.createElement("h5");
        let productDesc = document.createElement("p");
        let productPrice = document.createElement("p");
        let productLink = document.createElement("a");

        /* set differents attributes on the all elements of card */
        productCard.setAttribute("class", "card col-12 col-sm-6 col-md-4 col-lg");
        productImg.setAttribute("class", "thumbnailcard");
        productImg.setAttribute("alt", product.description);
        productImg.setAttribute("title", product.description);
        productImg.setAttribute("src", product.imageUrl);
        productInfos.classList.add("card-body");
        productTitle.classList.add("card-title");
        productDesc.classList.add("card-text");
        productPrice.classList.add("card-text");
        productLink.setAttribute("class", "btn btn-primary");
        productLink.setAttribute("href", "product.html?id=" + product._id);

        /* Put the differents elements of the card on their parent */
        listProducts.appendChild(productCard);
        productCard.appendChild(productImg);
        productCard.appendChild(productInfos);
        productInfos.appendChild(productTitle);
        productInfos.appendChild(productDesc);
        productInfos.appendChild(productPrice);
        productInfos.appendChild(productLink);

        /* Fill the differents elements which have need text */
        productTitle.textContent = product.name;
        productDesc.textContent = product.description;
        productPrice.textContent = product.price / 100 + " €";
        productLink.textContent = "En savoir plus";
    });
}

/* generates product card in the product page */
async function productDetails()
{
    idProduct = location.search.substring(4);
    const selectedProduct = await getProducts();
    if (idProduct === "")
    {
        document.getElementById("card-product").appendChild(document.createElement("h2")).textContent = "Erreur de connexion au service web !";
        return;
    }
    let container = document.getElementById("card-product");
    console.log("[Admin] -  You look at the product page id_" + selectedProduct._id);

    /* Generate all of elements of a card */
    let productCard = document.createElement("div");
    let productImg = document.createElement("img");
    let productDesc = document.createElement("div");
    let productTitle = document.createElement("h3");
    let productIntro = document.createElement("p");
    let productPrice = document.createElement("p");
    let productOptionTitle = document.createElement("select");
    let productAdd = document.createElement("button");

    /* set differents attributes on the all elements of card */
    productCard.setAttribute("class", "card col");
    productImg.classList.add("card-img-top");
    productImg.setAttribute("alt", "Image Produit");
    productImg.setAttribute("title", "Image Produit");
    productImg.setAttribute("src", selectedProduct.imageUrl);
    productDesc.classList.add("card-body");
    productTitle.classList.add("card-title");
    productIntro.classList.add("card-text");
    productPrice.setAttribute("class", "h5 mt-4 mb-2");
    productOptionTitle.setAttribute("class", "form-select");
    productAdd.setAttribute("class", "btn btn-outline-primary");
    productAdd.setAttribute("id", "product__add");

    /* Put the differents elements of the card on their parent */
    container.appendChild(productCard);
    productCard.appendChild(productImg);
    productCard.appendChild(productDesc);
    productDesc.appendChild(productTitle);
    productDesc.appendChild(productIntro);
    productDesc.appendChild(productOptionTitle);
    productDesc.appendChild(productPrice);
    selectedProduct.lenses.forEach((lense) =>
    {
        let optionProduit = document.createElement("option");
        productOptionTitle.appendChild(optionProduit).textContent = lense;
    });
    productDesc.appendChild(productAdd);

    /* Fill the differents elements which have need text */
    productTitle.textContent = selectedProduct.name;
    productIntro.textContent = selectedProduct.description;
    productPrice.textContent = selectedProduct.price / 100 + " €";
    productAdd.textContent = "Ajouter au panier";
}

/* Adds the product in the user's card */
async function addCard()
{
    await productDetails();
    let addToCardButton = document.getElementById("product__add");
    addToCardButton.addEventListener("click", async function(e)
    {
        e.preventDefault();
        const product = await getProducts();
        userCard.push(product);
        localStorage.setItem("userCard", JSON.stringify(userCard));
        console.log("[Admin] - The product has just been added to the card");
        alert("Vous avez ajouté ce produit dans votre panier");
    });
}

/* Displays user's card on the card page */
displayCard = () =>
{
    let i = 0;
    let totalPriceContent = 0;
    let cardBody = document.getElementById("cardbody");
    let totalPrice = document.getElementById("totalprice");
    if (JSON.parse(localStorage.getItem("userCard")).length > 0)
    {
        JSON.parse(localStorage.getItem("userCard")).forEach((product) =>
        {
            /* generates all elements of the arrayList of the card */
            let productRow = document.createElement("tr");
            let productNum = document.createElement("th");
            let productName = document.createElement("td");
            let productPrice = document.createElement("td");
            let productDelete = document.createElement("td");
            let productDeleteButton = document.createElement("button");

            /* attributes necessary attributes for elements */
            productRow.setAttribute("id", "rowProduct" + i);
            productNum.setAttribute("scope", "row");
            productDeleteButton.setAttribute("type", "button");
            productDeleteButton.classList.add("btn");
            productDeleteButton.classList.add("btn-outline-danger");
            productDeleteButton.setAttribute("id", "deleteproduct" + i);
            productDeleteButton.setAttribute("value", i);
            productDeleteButton.addEventListener('click', function (e)
            {
                e.preventDefault();
                const id = e.target.value;
                console.log("[Admin] - Delete item id = " + id);
                userCard.splice(id, 1);
                console.log("[Admin] - " + userCard);
                console.log("[Admin] - Delete localStorage");
                localStorage.clear();
                console.log("[Admin] - Rebuild userCard in localStorage");
                localStorage.setItem("userCard", JSON.stringify(userCard));
                console.log("[Admin] - rebuild card page");
                window.location.reload();
            });
            i++;

            /* Put the differents elements in their parents */
            cardBody.appendChild(productRow);
            productRow.appendChild(productNum);
            productRow.appendChild(productName);
            productRow.appendChild(productPrice);
            productRow.appendChild(productDelete);
            productDelete.appendChild(productDeleteButton);

            /* Fill HTML elements if they need */
            productNum.textContent = i;
            productName.textContent = product.name;
            productPrice.textContent = product.price / 100 + "€";
            productDeleteButton.textContent = "Supprimer ×";


            totalPriceContent += product.price;
        });
        totalPrice.innerHTML = totalPriceContent / 100 + "€";
    }
}

/* Checks if the form's inputs is good for API request */
checkInput = () =>
{
    /* Regex for control */
    const checkNumber = /[0-9]/;
    const checkEmail = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
    const checkSpecialChar = /[§!@#$%^&*(),.?":{}|<>]/;

    /* Message for end of control */
    let checkMessage = "";

    /* retrieve data from the form */
    let formLastName = document.getElementById("lastName").value;
    let formFirstName = document.getElementById("firstName").value;
    let formEmail = document.getElementById("email").value;
    let formAdress = document.getElementById("adress").value;
    let formCity = document.getElementById("city").value;

    /* Check all inputs */
    /* Check LastName */
    if (checkNumber.test(formLastName) === true || checkSpecialChar.test(formLastName) === true || formLastName === "")
    {
        checkMessage = "Vérifier/Renseigner votre nom"
    }
    else
    {
        console.log("[Admin] - Check pass for LastName")
    }

    /* Check FirstName */
    if (checkNumber.test(formFirstName) === true || checkSpecialChar.test(formFirstName) === true || formFirstName === "")
    {
        checkMessage = checkMessage + "\n" + "Vérifier/Renseigner votre prénom";
    }
    else
    {
        console.log("[Admin] - Check pass for FirstName")
    }

    /* Check Email */
    if (checkEmail.test(formEmail) === false || formEmail === "")
    {
        checkMessage = checkMessage + "\n" + "Vérifier/Renseigner les emails";
    }
    else
    {
        console.log("[Admin] - Check pass for emails");
    }

    /* Check Address */
    if (checkSpecialChar.test(formAdress) === true || formAdress === "")
    {
        checkMessage = checkMessage + "\n" + "Vérifier/Renseigner votre adresse";
    }
    else
    {
        console.log("[Admin] - Check pass for adress");
    }

    /* Check City */
    if (checkNumber.test(formCity) === true || checkSpecialChar.test(formCity) === true || formCity === "")
    {
        checkMessage = checkMessage + "\n" + "Vérifier/Renseigner votre ville";
    }
    else
    {
        console.log("[Admin] - Check pass for City");
    }

    /* If one input doesn't pass check - Alert */
    if (checkMessage !== "")
    {
        alert("Il est nécessaire de : " + "\n" + checkMessage);
        return false;
    }
    /* Generate contact object for API request */
    else
    {
        contact =
            {
                firstName: formFirstName,
                lastName: formLastName,
                address: formAdress,
                city: formCity,
                email: formEmail
            };
        return contact;
    }
}

/* Checks if the user's card is good for API request */
checkCard = () =>
{
    let cardStatus = JSON.parse(localStorage.getItem("userCard"));
    /* check if card exist */
    if (cardStatus === null)
    {
        alert("il y a eu un problème avec votre panier, veuillez recharger la page et réessayer");
        return false;
    }
    /* Check if card have at least 1 item */
    else if (cardStatus.length < 1)
    {
        console.log("[Admin] - ERROR => LocalStorage doesn't contain user's card");
        alert("Votre panier est vide !");
        return false;
    }
    /* make the array for API request */
    else
    {
        console.log("[Admin] - User's card contain items");
        JSON.parse(localStorage.getItem("userCard")).forEach((product) =>
        {
           products.push(product._id);
        });
        console.log("[Admin] - This array will be send to API : " + products);
        return true;
    }
}

/* API Request */
sendDataToApi = (objectRequest) =>
{
    return new Promise((resolve) =>
    {
       let request = new XMLHttpRequest();
       request.onreadystatechange = function ()
       {
           if (this.readyState === XMLHttpRequest.DONE && this.status === 201)
           {
               sessionStorage.setItem("order", this.responseText);
               document.forms["commandForm"].action = "./confirm-order.html";
               document.forms["commandForm"].submit();
               resolve(JSON.parse(this.responseText));
           }

       }
       request.open("POST", APIURL + "order");
       request.setRequestHeader("Content-Type", "application/json");
       request.send(objectRequest);
    });
}

/* centralize all checks and make the API Request */
validForm = () =>
{
    let sendBtn = document.getElementById("sendBtn");
    sendBtn.addEventListener("click", function (e)
    {
        e.preventDefault();
        if (checkCard() && checkInput() !== false)
        {
            console.log("[Admin] - All ok for send datas to API");
            let object =
                {
                    contact,
                    products
                };
            console.log("[Admin] - " + object);
            let objectRequest = JSON.stringify(object);
            console.log("[Admin] - " + objectRequest);
            sendDataToApi(objectRequest);

            contact = {};
            products = [];
            localStorage.clear();
        }
        else
        {
            console.log("[Admin] - Error !");
        }
    });
}

/* Diplays orderID on the confirm-order page */
resultOrder = () =>
{
    /* Check if an order is in course */
    if (sessionStorage.getItem("order") !== null)
    {
        console.log(sessionStorage.getItem("order"));
        let order = JSON.parse(sessionStorage.getItem("order"));
        document.getElementById("lastName").innerHTML = order.contact.lastName;
        document.getElementById("orderID").innerHTML = order.orderId;
        sessionStorage.removeItem("order");
    }
    /* redirect to index */
    else
    {
        alert("Aucune commande passée, vous êtes arrivé ici par erreur !");
        window.location.replace("./index.html");
    }
}
