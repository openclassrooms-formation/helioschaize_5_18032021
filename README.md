# Orinoco #

Code source de l'application Orinoco, développé dans le cadre de la formation OpenClasrooms "Developpeur Web"

### Prérequis ###

Vous devez avoir Node et `npm` installé en local sur votre machine.

### Installation ###

Clonez ce repository. Placez-vous dans le dossier `back` et exécutez `npm install`.  
Après cela vous pouvez exécuter `node server`. 
L'API tournera sur `localhost` avec comme port par défaut `3000`.  
Si l'API tourne sur un autre port pour une quelconque raison, cela sera indiqué dans la console.  
Ex : `Listening on port 3001`.
